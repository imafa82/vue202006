const authService = {
    data() {
        return {
            token: undefined,
            user: undefined
        }
    },
    methods:{
        setLocalStorage: function () {
            if(localStorage.getItem('token')){
                this.setToken(localStorage.getItem('token'));
                this.setUser();
            }
        },
        setToken(token){
            this.token = token;
            localStorage.setItem('token', this.token);
        },
        setUser: function () {
            this.$http.get('https://www.massimilianosalerno.it/jwt/api/user').then(res => {
               this.user = res && res.body? res.body: undefined;
            });
        },
        logout() {
            localStorage.removeItem('token');
            this.$router.push({name: 'Login'});
        }
    }
};

export default authService;
