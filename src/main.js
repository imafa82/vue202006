import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all'
import 'bootstrap/dist/css/bootstrap.css'
import 'popper.js'
import 'jquery'
import 'bootstrap'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Card from "./components/Card";
import Header from "./components/Header";
import VueTouch from "vue-touch";
import VueResource from 'vue-resource';
import authService from './services/authService';
import authInterceptors from "./interceptors/authInterceptors";

Vue.config.productionTip = false
Vue.use(VueResource);
Vue.http.options.root =  'https://jsonplaceholder.typicode.com/';
Vue.http.options.headers = {
  'Content-type': 'application/json'
};
Vue.http.interceptors.push(authInterceptors);
router.beforeEach((to, from, next) => {
  console.log(to, from);
  next();
});
Vue.use(VueTouch);
Vue.mixin(authService);
Vue.component('app-card', Card);
Vue.component('app-header', Header);
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
