import Login from "../views/Login";
import AuthPage from "../views/auth/AuthPage";
import authRoutes from "./auth";
import AuthGuard from "../guards/authGuard";
import NoAuthGuard from "../guards/noAuthGuard";

const routes = [
    {
        path: '/login',
        name: 'Login',
        beforeEnter: NoAuthGuard,
        component: Login
    },
    {
        path: '',
        component: AuthPage,
        beforeEnter: AuthGuard,
        children: authRoutes
    },
    {
        path: '*',
        redirect: {name: 'Home'}
    }

];

export default routes;
